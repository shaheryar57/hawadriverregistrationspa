import React from "react";
import "antd/dist/antd.css";
import { Form, Input, Select, Row, Col, Button } from "antd";
import { ValidationRules } from "../../utils/validation";

export const VehicleDetailsForm = ({ index, handleNextStep }) => {
  const [form] = Form.useForm();
  const onFinish = (values) => {
    localStorage.setItem("vehicleData", JSON.stringify(values));
    const step = localStorage.getItem("current_step");
    localStorage.setItem("current_step", Number(step) + 1);
    handleNextStep();
  };
  return (
    <div>
      <Form
        layout="vertical"
        size="large"
        name="basic"
        form={form}
        style={{ marginTop: "2rem" }}
        onFinish={onFinish}
      >
        <Row gutter={16}>
          <Col span={12}>
            <Form.Item
              label="Vehicle Type"
              name="vehicleType"
              rules={ValidationRules.required}
              required
            >
              <Select>
                <Select.Option value="FAW">FAW</Select.Option>
                <Select.Option value="Deawoo">Deawoo</Select.Option>
                <Select.Option value="Datsun">Datsun</Select.Option>
              </Select>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label="Vehicle Model"
              name="vehicleModel"
              rules={ValidationRules.required}
              required
            >
              <Select>
                <Select.Option value="Bestturn B70">Bestturn B70</Select.Option>
                <Select.Option value="Bestturn B50">Bestturn B50</Select.Option>
                <Select.Option value="City Golf">City Golf</Select.Option>
              </Select>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={16}>
          <Col span={12}>
            <Form.Item
              label="Year of Manufacturing"
              name="yearofManufacturing"
              required
              rules={ValidationRules.year}
            >
              <Input placeholder="Manufacture year" />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label="vehicle Number"
              name="vehicleNumber"
              rules={ValidationRules.required}
            >
              <Input placeholder="Vehicle Number" />
            </Form.Item>
          </Col>
        </Row>
        <Button type="primary" style={{ float: "right" }} htmlType="submit">
          Next
        </Button>
      </Form>
    </div>
  );
};
