import React, { useState } from "react";
import "antd/dist/antd.css";
import { Row, Col, Button } from "antd";
import axios from "axios";
export const UpladDocumentsForm = ({
  index,
  uploadedDocumentsImages,
  handleNextStep,
  handleSubmitForm,
}) => {
  const [previewProfileImage, setPreviewProfileImage] = useState(null);
  const [previewDriverlicenseImage, setDriverlicenseImage] = useState(null);
  const [previewNationalId, setNationalId] = useState(null);
  const [previewCarBackSide, setCarBackSide] = useState(null);
  const [previewCarFrontSide, setCarFrontSide] = useState(null);
  const [previewVehicleRegistration, setVehicleRegistration] = useState(null);
  const profile = React.useRef(null);
  const driverLicense = React.useRef(null);
  const nationalId = React.useRef(null);
  const carBackSide = React.useRef(null);
  const carFrontSide = React.useRef(null);
  const vehicleRegistration = React.useRef(null);
  const [imageUploadLoader, setImageUploadLoader] = useState(false);
  const [imageUploadButtonStatus, setImageUploadButtonDisable] = useState(true);
  let [imagesArray, sendToImagesArray] = useState([]);

  const token = localStorage.getItem("token");
  const parsedToken = JSON.parse(token);

  const setPreview = (name, file) => {
    switch (name) {
      case "profile":
        return setPreviewProfileImage(file);
      case "driverLicense":
        return setDriverlicenseImage(file);
      case "nationalId":
        return setNationalId(file);
      case "carBackSide":
        return setCarBackSide(file);
      case "carFrontSide":
        return setCarFrontSide(file);
      case "vehicleRegistration":
        return setVehicleRegistration(file);
      default:
        return;
    }
  };

  const onChange = async (event) => {
    const fileUploaded = event.target.files[0];
    setImage(event);
  };
  const handleClicked = (event, typee) => {
    if (typee === "profile") {
      profile.current.click();
    }
    if (typee === "driverLicense") {
      driverLicense.current.click();
    }
    if (typee === "nationalId") {
      nationalId.current.click();
    }
    if (typee === "carBackSide") {
      carBackSide.current.click();
    }
    if (typee === "carFrontSide") {
      carFrontSide.current.click();
    }
    if (typee === "vehicleRegistration") {
      vehicleRegistration.current.click();
    }
  };

  const imagesUploaderOnS3 = async (event) => {
    setImageUploadLoader(true);
    for (let image of imagesArray) {
      // var response = await getSignedUrl({
      //   variables: {
      //     extension: image.type,
      //   },
      //   context: {
      //     headers: {
      //       token: parsedToken,
      //     },
      //   },
      // });
      var response = await axios.post(
        "http://localhost:3040/graphql",
        {
          query: `query  getSignedUrl( $extension:Extension!) {
          getSignedUrl(
            extension:$extension
            )
        }`,
          variables: {
            extension: image.type,
          },
        },
        {
          headers: {
            token: parsedToken,
          },
        }
      );

      const options = {
        headers: {
          "Content-Type": image.type,
          ImageType: image.imageFor,
        },
      };

      await axios
        .put(`${response.data.data.getSignedUrl}`, image.name, options)
        .then((res) => {
          var list = res.config.url.split("?");
          var imageURL = list[0];

          assignPhoto(res.config.headers.ImageType, imageURL);
        });
    }
  };

  var photoList = [];

  const assignPhoto = (imageType, imageURL) => {
    photoList.push({ imageFor: imageType, image: imageURL });
    if (photoList.length === 6) {
      localStorage.setItem("documents", JSON.stringify(photoList));
      uploadedDocumentsImages(photoList);
      handleSubmitForm();
      handleNextStep();
      setImageUploadLoader(false);
    }
  };

  function setImage(event) {
    const name = event.target.files[0];
    // const name = `${event.target.name}` + event.target.files[0];
    const file = URL.createObjectURL(event.target.files[0]);
    const type = getType(event.target.files[0]);
    setPreview(event.target.name, file);
    imagesArray = imagesArray.filter(
      (image) => image.imageFor != event.target.name
    );

    sendToImagesArray([
      ...imagesArray,
      { imageFor: event.target.name, image: file, name, type },
    ]);
    checkDisablesStatus();
  }

  function getType(file) {
    var list = file.type.split("/");

    const type = list[list.length - 1];

    return type;
  }

  const checkDisablesStatus = () => {
    if (imagesArray.length === 5) {
      setImageUploadButtonDisable(false);
    }
  };
  return (
    <>
      <div className="personalImageUplad">
        <Row justify="center" style={{ marginTop: "12px" }}>
          <Col sm={8}>
            <div className="profileImage">
              <img
                alt=""
                width="200px"
                height="200px"
                src={previewProfileImage}
                onClick={(event) => handleClicked(event, "profile")}
              />
            </div>
            <label> Upload Profile Image</label>
          </Col>
          <Col sm={8}>
            <div className="driverLicenseImage">
              <img
                alt=""
                width="200px"
                height="200px"
                src={previewDriverlicenseImage}
                onClick={(event) => handleClicked(event, "driverLicense")}
              />
            </div>
            <label> Driver License</label>
          </Col>
          <Col sm={8}>
            <div className="nationalIdCard">
              <img
                alt=""
                width="200px"
                height="200px"
                src={previewNationalId}
                onClick={(event) => handleClicked(event, "nationalId")}
              />
            </div>
            <label> National Id Card</label>
          </Col>
        </Row>

        {/* second row */}
        <Row justify="center" style={{ marginTop: "12px" }}>
          <Col sm={8}>
            <div className="profileImage">
              <img
                alt=""
                width="200px"
                height="200px"
                src={previewCarBackSide}
                onClick={(event) => handleClicked(event, "carBackSide")}
              />
            </div>
            <label> Back Side of Car</label>
          </Col>
          <Col sm={8}>
            <div className="frontSideOfCar">
              <img
                alt=""
                width="200px"
                height="200px"
                src={previewCarFrontSide}
                onClick={(event) => handleClicked(event, "carFrontSide")}
              />
            </div>
            <label> Front of Car</label>
          </Col>
          <Col sm={8}>
            <div className="vehicleRegistraation">
              <img
                alt=""
                width="200px"
                height="200px"
                src={previewVehicleRegistration}
                onClick={(event) => handleClicked(event, "vehicleRegistration")}
              />
            </div>
            <label>Vehicle Registration</label>
          </Col>
        </Row>
        <div className="">
          <input
            type="file"
            id="file-input"
            onChange={onChange}
            ref={profile}
            name="profile"
            style={{ display: "none" }}
          />
        </div>
        {/* driver license image */}

        <div className="">
          <input
            type="file"
            id="file-input"
            onChange={onChange}
            ref={driverLicense}
            name="driverLicense"
            style={{ display: "none" }}
          />
        </div>

        {/* nationId */}

        <div className="">
          <input
            type="file"
            id="file-input"
            onChange={onChange}
            ref={nationalId}
            name="nationalId"
            style={{ display: "none" }}
          />
        </div>

        {/* carbackSide */}

        <input
          type="file"
          id="file-input"
          onChange={onChange}
          ref={carBackSide}
          name="carBackSide"
          style={{ display: "none" }}
        />

        {/* Car front side */}

        <input
          type="file"
          id="file-input"
          onChange={onChange}
          ref={carFrontSide}
          name="carFrontSide"
          style={{ display: "none" }}
        />

        {/* vehicleRegistration */}

        <input
          type="file"
          id="file-input"
          onChange={onChange}
          ref={vehicleRegistration}
          name="vehicleRegistration"
          style={{ display: "none" }}
        />

        <Button
          style={{ float: "right" }}
          type="primary"
          loading={imageUploadLoader}
          disabled={imageUploadButtonStatus}
          htmlType="submit"
          onClick={(event) => imagesUploaderOnS3(event)}
        >
          Upload Documents
        </Button>
      </div>
    </>
  );
};
