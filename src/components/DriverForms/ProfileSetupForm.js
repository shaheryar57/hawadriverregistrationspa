import React, { useState, useEffect } from "react";
import "antd/dist/antd.css";
import "react-notifications/lib/notifications.css";
import { Form, Input, Select, DatePicker, Row, Col, Button } from "antd";
import { ValidationRules } from "../../utils/validation";
import { useMutation } from "@apollo/client";
import { LOGIN } from "../../Graphql/Mutation";

export const ProfileSetupForm = ({
  index,
  handleNextStep,
  profileSubmittedValues,
}) => {
  const [token, setToken] = useState("");
  const [login, { error }] = useMutation(LOGIN, {
    onError: (errors) => {
      console.log("error", errors);
    },
  });
  useEffect(() => {
    const user = localStorage.getItem("user");
    const currentUser = JSON.parse(user);
    async function getToken() {
      const response = await login({
        variables: {
          userType: "Driver",
          phoneNumber: currentUser.phoneNumber,
        },
      });

      setToken(response.data.login.token);
      localStorage.setItem("token", JSON.stringify(response.data.login.token));
    }
    getToken();
  }, []);

  const [form] = Form.useForm();

  const onFinish = async (values) => {
    localStorage.setItem("profileData", JSON.stringify(values));
    const step = localStorage.getItem("current_step");
    localStorage.setItem("current_step", Number(step) + 1);
    handleNextStep();
  };
  return (
    <div>
      <Form
        layout="vertical"
        size="large"
        name="basic"
        form={form}
        onFinish={onFinish}
        style={{ marginTop: "2rem" }}
      >
        <Row gutter={16}>
          <Col span={8}>
            <Form.Item
              label="Fist Name"
              name="firstName"
              rules={ValidationRules.required}
            >
              <Input
              //onChange={(e)=>setFirstName(e.target.value)}
              />
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item
              label="Last Name"
              name="lastName"
              rules={ValidationRules.required}
            >
              <Input
              //onChange={(e)=>setFirstName(e.target.value)}
              />
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item
              label="National Id"
              name="nationalId"
              rules={ValidationRules.required}
            >
              <Input
              //onChange={(e)=>setFirstName(e.target.value)}
              />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={16}>
          <Col span={8}>
            <Form.Item
              label="Password"
              name="password"
              rules={ValidationRules.password}
            >
              <Input
                type="password"
                //onChange={(e)=>setFirstName(e.target.value)}
              />
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item
              label="Confirm Password"
              name="confirmPassword"
              rules={ValidationRules.confirmPassword}
            >
              <Input
                type="password"
                //onChange={(e)=>setFirstName(e.target.value)}
              />
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item
              label="Citizenship"
              name="citizenship"
              rules={ValidationRules.required}
            >
              <Input
              //onChange={(e)=>setFirstName(e.target.value)}
              />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={16}>
          <Col span={10}>
            <Form.Item
              label="Region i'm working in"
              name="livingRegion"
              rules={ValidationRules.required}
              required
            >
              <Select>
                <Select.Option value="Pakistan">Pakistan</Select.Option>
                <Select.Option value="India">India</Select.Option>
                <Select.Option value="UAE">UAE</Select.Option>
              </Select>
            </Form.Item>
          </Col>

          <Col span={10}>
            <Form.Item
              label="City i'm working in"
              name="workingRegion"
              rules={ValidationRules.required}
              required
            >
              <Select>
                <Select.Option value="Pakistan">Lahore</Select.Option>
                <Select.Option value="India">Multan</Select.Option>
                <Select.Option value="UEA">Karachi</Select.Option>
              </Select>
            </Form.Item>
          </Col>

          <Col span={4}>
            <Form.Item label="Date of Birth" name="DateOfBirth" required>
              {/* <DatePicker onChange={(date, dateString)=>setDateOfBirth(dateString)}
           /> */}
              <DatePicker />
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={16}></Row>
        {/* <Button   type="primary" onClick={() => handleNextStep()}>
            Custom Next Button
          </Button> */}
        <Button style={{ float: "right" }} type="primary" htmlType="submit">
          Next
        </Button>
      </Form>
    </div>
  );
};
