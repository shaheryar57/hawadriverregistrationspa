import React from "react";
import "antd/dist/antd.css";
import { Card } from "antd";
import { MultiStepDriverForm } from "./MultiStepDriverRegistrationForm";
import { useMutation } from "@apollo/client";
import { NotificationManager } from "react-notifications";
import { CREATEDRIVER } from "../Graphql/Mutation";

export const RegistrationFormDriver = () => {
  const [createDriver, { error }] = useMutation(CREATEDRIVER, {
    onError: (errors) => {
      console.log("error", errors);
    },
  });
  const createDriverFormSubmit = async (
    user,
    profileData,
    vehicleData,
    documents
  ) => {
    const resp = await createDriver({
      variables: {
        userId: user.id,
        firstName: profileData.lastName,
        lastName: profileData.lastName,
        nationalId: profileData.nationalId,
        citizenship: profileData.citizenship,
        dateOfBirth: profileData.DateOfBirth,
        vehicleType: vehicleData.vehicleType,
        vehicleModel: vehicleData.vehicleModel,
        yearOfManufacture: vehicleData.yearofManufacturing,
        vehiclePlateLetter: vehicleData.vehicleNumber,
        vehiclePlateNo: vehicleData.vehicleNumber,
        workingRegion: profileData.workingRegion,
        livingRegion: profileData.livingRegion,
        licenseImage: documents.filter(
          (document) => document.imageFor === "driverLicense"
        )[0].image,
        carFrontImage: documents.filter(
          (document) => document.imageFor === "carFrontSide"
        )[0].image,
        carBackImage: documents.filter(
          (document) => document.imageFor === "carBackSide"
        )[0].image,
        nationalIdImage: documents.filter(
          (document) => document.imageFor === "nationalId"
        )[0].image,
        vehicleRegistrationImage: documents.filter(
          (document) => document.imageFor === "vehicleRegistration"
        )[0].image,
        password: profileData.password,
      },
      context: {
        headers: {
          token: JSON.parse(localStorage.getItem("token")),
        },
      },
    });
    console.log(resp);
    if (resp.errors?.message) {
      NotificationManager.error(`${resp.errors?.message} `);
    } else {
      const step = localStorage.getItem("current_step");
      localStorage.setItem("current_step", Number(step) + 1);
    }
  };

  return (
    <div
      className="site-card-border-less-wrapper"
      style={{ padding: "30px", background: "#ececec" }}
    >
      <Card
        title={
          <h3 style={{ display: "flex", justifyContent: "center" }}>
            Driver Registration Form
          </h3>
        }
        bordered={false}
      >
        <MultiStepDriverForm createDriverFormSubmit={createDriverFormSubmit} />
      </Card>
    </div>
  );
};
