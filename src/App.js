import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider,
  HttpLink,
  from,
} from "@apollo/client";
import { onError } from "@apollo/client/link/error";
import { RegistrationFormDriver } from "./components/registrationFormDriver";
import { AppPageHeader } from "./components/appPageHeader";
import { NotificationContainer } from "react-notifications";
const errorLink = onError(({ graphqlErrors, newtwoorkErrors }) => {
  if (graphqlErrors) {
    graphqlErrors.map(({ message, location, path }) => {
      console.log(`Graphql error ${message}`);
    });
  }
});
const link = from([
  errorLink,
  new HttpLink({ uri: " http://localhost:3040/graphql" }),
]);

const client = new ApolloClient({
  cache: new InMemoryCache(),
  link: link,
});

function App() {
  return (
    <div className="App">
      <ApolloProvider client={client}>
        <AppPageHeader />
        <RegistrationFormDriver />
      </ApolloProvider>
      <NotificationContainer />
    </div>
  );
}

export default App;
