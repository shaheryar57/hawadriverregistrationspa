import React, {useState} from 'react'
import 'antd/dist/antd.css'; 
import { NotificationManager} from 'react-notifications';
import 'react-notifications/lib/notifications.css';
import {Form,
    Input,
    Row, Col, Button } from 'antd';
    import { ValidationRules } from "../../utils/validation";
    import {useMutation} from '@apollo/client';
    import { REGISTER} from '../../Graphql/Mutation';

export const RegisterationForm = ({index, handleNextStep,submittedValues}) =>
{
    console.log(index);
    const [form] = Form.useForm();
    const [profileSubmitButtonLoading, setProfileSubmitButtonLoading] = useState(false)
    const [register, {error}] = useMutation(REGISTER, {onError: (errors)=>{
      console.log("error",errors);
    }});
    const onFinish= async (values)=>{
     
      setProfileSubmitButtonLoading(true)
       const registerNewUser= await register(
          {
            variables:{
              email:values.email,
              name:values.name,
              phoneNumber:values.phoneNumber,
              type: 'Driver'
            }
          }
        )
        if(registerNewUser?.errors?.message)
        {
          
          NotificationManager.error(`${registerNewUser.errors.message} user exist!`)
     
          setProfileSubmitButtonLoading(false)
        }
        else{
          console.log('register new user data',registerNewUser.data.register)
          localStorage.setItem('user',JSON.stringify(registerNewUser.data.register))
          localStorage.setItem('current_step',0)
          handleNextStep()
          setProfileSubmitButtonLoading(false)
        }
       
    }
    return (
        <div>
            <Form
        // labelCol={{span: 8,}}
        // labelRow={{span: 8}}
        // wrapperCol={{pan: 12,}}
        layout='vertical'
        size="large"
        name="basic"  
        form={form}
        onFinish={onFinish}
        style={{marginTop:'2rem'}}
      >
        <Row gutter={16}>
        <Col span={8}>
            <Form.Item label="Phone Number" name="phoneNumber" rules={ValidationRules.mobile}
            >
            <Input 
            //onChange={(e)=>setFirstName(e.target.value)} 
            />
            </Form.Item>
        </Col>
        <Col span={8}>
            <Form.Item label="Email" name="email" rules={ValidationRules.email}
            >
            <Input 
            //onChange={(e)=>setFirstName(e.target.value)} 
            />
            </Form.Item>
        </Col>
        <Col span={8}>
        <Form.Item label="Name" name="name" rules={ValidationRules.required}>
          <Input
          placeholder="Name"
        
          />
        </Form.Item>
        </Col>
        </Row>
     
           {/* <Button   type="primary" onClick={() => handleNextStep()}>
            Custom Next Button
          </Button> */}
          <Button  style={{float:'right'}} type="primary" htmlType="submit" loading={profileSubmitButtonLoading}  >
                    Next
                </Button>
           {/* <button>Submit </button> */}
          

      </Form>
        </div>
    )
}
