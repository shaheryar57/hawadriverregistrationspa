 import {gql} from '@apollo/client';

// export const CREATE_DRIVER_MUTATION = gql`

// mutation login($userType:UserType! $deviceToken: String! $password:String! $email:String!) {
//     login(
//         userType:$userType
//         deviceToken:$deviceToken
//         password:$password
//          email:$email
//     )
//     {
//       id
//       email
//     }
// }`

export const REGISTER = gql`
mutation register( $type:UserType! $email:String! $name: String!  $phoneNumber:String!) {
  register(
       phoneNumber: $phoneNumber,
       create:{
        email: $email
        name: $name
         type: $type
       }  
    )
    {
      id
      email
      phoneNumber
    }
}`

export const LOGIN =  gql`
mutation login( $userType:UserType!  $phoneNumber:String!) {
  login(
        userType:$userType
        phoneNumber:$phoneNumber
       
    )
    {
      token
    }
}
`

// export const SINGEDURL = gql`
// mutation  getSignedUrl( $extension:Extension!) {
//   getSignedUrl(
//     extension:$extension
//     )
// }
// ` 

export const SINGEDURL = gql`
query  getSignedUrl( $extension:Extension!) {
  getSignedUrl(
    extension:$extension
    )
}
`

export const CREATEDRIVER = gql`
mutation createDriver($userId: Int! $citizenship:String $nationalId:String  $firstName:String  $lastName:String  $dateOfBirth:String! $vehicleType:String! $vehicleModel:String! $yearOfManufacture:String! $vehiclePlateLetter:String! $vehiclePlateNo:String! $workingRegion:String! $livingRegion:String! $licenseImage:String $carFrontImage:String $carBackImage:String $nationalIdImage:String $vehicleRegistrationImage:String $password:String!) {
  createDriver(
    create:{
        firstName:$firstName
        lastName:$lastName
        nationalId:$nationalId
        userId:$userId
        citizenship:$citizenship
        dateOfBirth:$dateOfBirth
        vehicleType:$vehicleType
        vehicleModel:$vehicleModel
        yearOfManufacture:$yearOfManufacture
        vehiclePlateLetter:$vehiclePlateLetter
        vehiclePlateNo:$vehiclePlateNo
        workingRegion:$workingRegion
        livingRegion:$livingRegion
        licenseImage:$licenseImage
        carFrontImage:$carFrontImage
        carBackImage:$carBackImage
        nationalIdImage:$nationalIdImage
        vehicleRegistrationImage:$vehicleRegistrationImage
        password:$password
       }  
    )
    {
      id
    }
}`