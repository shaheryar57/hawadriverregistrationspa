import React, { useState, useEffect } from "react";
import "antd/dist/antd.css";
import { Steps, Button, message, Result } from "antd";
import { SmileOutlined } from "@ant-design/icons";
import { RegisterationForm } from "./DriverForms/RegisterationForm";
import { ProfileSetupForm } from "./DriverForms/ProfileSetupForm";
import { VehicleDetailsForm } from "./DriverForms/VehicleDetailsForm";
import { UpladDocumentsForm } from "./DriverForms/UploadDocumentsForm";
const { Step } = Steps;

const steps = [
  {
    step: 0,
    title: "Registration",
    content: <RegisterationForm index={0} />,
  },
  {
    step: 0,
    title: "Profile Setup",
    content: <ProfileSetupForm index={1} />,
  },
  {
    step: 1,
    title: "Vehicle Details",
    content: <VehicleDetailsForm index={2} />,
  },
  {
    step: 2,
    title: "Documents",
    content: <UpladDocumentsForm />,
  },
];

export const MultiStepDriverForm = ({ createDriverFormSubmit }) => {
  const [current, setCurrent] = React.useState(0);
  const [profileData, setProfileData] = useState("");
  const [uploadedDocuments, setUploadedDocuments] = useState([]);
  useEffect(() => {
    const step = localStorage.getItem("current_step");
    if (step) {
      setCurrent(Number(step) + 1);
    }
  });

  const next = () => {
    setCurrent(current + 1);
  };

  const prev = () => {
    setCurrent(current - 1);
  };

  const handleNextStep = () => {
    setCurrent(current + 1);
  };

  const profileSubmittedValues = (values) => {
    setProfileData(values);
  };

  const uploadedDocumentsImages = (values) => {
    setUploadedDocuments(values);
  };
  const handleSubmitForm = () => {
    const user = retriveItem("user");
    const profileData = retriveItem("profileData");
    const vehicleData = retriveItem("vehicleData");
    const documents = retriveItem("documents");

    createDriverFormSubmit(user, profileData, vehicleData, documents);
  };

  const retriveItem = (key) => {
    return JSON.parse(localStorage.getItem(key));
  };

  return (
    <>
      <Steps current={current}>
        {steps.map((item) => (
          <Step key={item.title} title={item.title} />
        ))}
      </Steps>
      <div className="steps-content">
        {current === 0 ? (
          <RegisterationForm index={0} handleNextStep={handleNextStep} />
        ) : current === 1 ? (
          <ProfileSetupForm
            index={1}
            profileSubmittedValues={profileSubmittedValues}
            handleNextStep={handleNextStep}
          />
        ) : current === 2 ? (
          <VehicleDetailsForm index={2} handleNextStep={handleNextStep} />
        ) : current === 3 ? (
          <UpladDocumentsForm
            index={3}
            uploadedDocumentsImages={uploadedDocumentsImages}
            handleNextStep={handleNextStep}
            handleSubmitForm={handleSubmitForm}
          />
        ) : current === 4 ? (
          <Result
            icon={<SmileOutlined />}
            title="Great, we have done all the operations!"
            extra={
              <Button onClick={() => localStorage.clear()} type="primary">
                Next
              </Button>
            }
          />
        ) : (
          <Result
            status="500"
            title="500"
            subTitle="Sorry, something went wrong."
            extra={
              <Button type="primary" onClick={() => localStorage.clear()}>
                Back Home
              </Button>
            }
          />
        )}
      </div>
      <div className="steps-action">
        {/* {current < steps.length - 1 && (
          <Button  style={{float:'right'}} type="primary" onClick={() => next()}>
            Next
          </Button>
        )} */}

        {/* {current > 0 && (
        //   <Button style={{ margin: '0 8px' }} onClick={() => prev()}>
        //     Previous
        //   </Button>
        )} */}
      </div>
    </>
  );
};
