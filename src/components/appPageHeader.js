import React from 'react'
import { PageHeader } from 'antd';
import haeaLogo from '../asserts/hawaLogo.svg';
import 'antd/dist/antd.css'; 

export const AppPageHeader = () => {
    return (
        <div>
            <PageHeader
    className="site-page-header"
    title={ <h1 style={{color: '#87CEEB'}}>Hawa</h1> }
    avatar={{ src: haeaLogo, size:100 } }
  />
        </div>
    )
}

